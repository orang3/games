;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages ungrounded)
  #:use-module (nonguix build-system binary)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages compression)
  #:use-module (games game-local-fetch)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public ungrounded
  (package
    (name "ungrounded")
    (version "1.01")
    (source
     (origin
      (method game-local-fetch)
      (uri "Ungrounded-linux-1.01.zip")
      (sha256
       (base32
        "171a7q3fnn71r2y47rdzmxi31l9aw2k1bdmx725q7hl5qgm7a4pi"))))
    (build-system binary-build-system)
    (arguments
     `(#:install-plan
       `(("Ungrounded.love" "share/ungrounded/")
         ("ungrounded" "bin/"))
       #:phases
       (modify-phases %standard-phases
         (replace 'unpack
           (lambda* (#:key inputs #:allow-other-keys)
             ;; The standard unpack fails because there is no directory in the
             ;; archive.
             (invoke "unzip" (assoc-ref inputs "source"))
             #t))
         (add-before 'install 'create-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((ungrounded-wrapper "ungrounded"))
               (with-output-to-file ungrounded-wrapper
                 (lambda _
                   (format #t "#!~a~%" (which "bash"))
                   (format #t "~a/bin/love ~a/share/ungrounded/Ungrounded.love"
                           (assoc-ref inputs "love") (assoc-ref outputs "out"))))
               (chmod ungrounded-wrapper #o755))
             #t))
           (add-after 'install 'make-desktop-entry
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (make-desktop-entry-file
                   (string-append out "/share/applications/ungrounded.desktop")
                   #:name "Ungrounded"
                   #:icon (assoc-ref inputs "icon")
                   #:exec (string-append out "/bin/ungrounded")
                   #:categories '("Application" "Game")))
               #t)))))
    (inputs
     `(("love" ,love)))
    (native-inputs
     `(("unzip" ,unzip)
       ("icon" ,(origin
                  (method url-fetch)
                  (uri "https://img.itch.zone/aW1hZ2UvMTE0NDAxLzUyOTgzOS5wbmc=/original/51UBeX.png")
                  (sha256
                   (base32
                    "1ahyj8qsfi0gv7lclrk69yd5di1p1v9azc607626727rwsl9hhq2"))))))
    (home-page "https://zenzoa.itch.io/ungrounded")
    (synopsis "Zen game")
    (description "Plant trees and climb through their branches to reach the
sun.  Select for the beautiful, the strange, the fantastic - and use your
platforming skills to navigate the custom level you'll grow from scratch.

If you fall too far or get stuck, you can use your superpower ghosting
ability to hop through the trees with ease - for a little while, at least.
Being a tree ghost is exhausting!

The trees are secretly L-systems that mutate with each generation.  You get
points for the greater diversity you accumulate in your forest.  Race to the
top, or cultivate your garden - it's your choice.")
    (license (undistributable "No license file"))))
