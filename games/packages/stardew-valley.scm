(define-module (games packages stardew-valley)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (gnu packages gcc)
  #:use-module (nonguix licenses)
  #:use-module (games gog-download)
  #:use-module (games build-system mojo))

(define-public gog-stardew-valley
  (package
    (name "gog-stardew-valley")
    (version "1.6.8.24119.6732702600")
    (source
     (origin
       (method gog-fetch)
       (uri "gogdownloader://stardew_valley/en3installer0")
       (file-name (string-append "stardew_valley_" (string-replace-substring version "." "_") "_72964.sh"))
       (sha256 (base32 "0g8hp95sj6bkx0bqkjfyvgcrgg8ad5z69frm9zami1m7100jkcfl"))))
    (arguments `(#:patchelf-plan
                 (map (lambda (binary)
                        (list binary '("gcc:lib")))
                      '("Stardew Valley"))))
    (inputs
     `(("gcc:lib" ,gcc "lib")))
    (build-system mojo-build-system)
    (supported-systems '("x86_64-linux"))
    (home-page "https://www.stardewvalley.net/")
    (synopsis "farm life sim video game developed by Eric \"ConcernedApe\" Barone")
    (description "Players take the role of a character who inherits their
deceased grandfather's dilapidated farm in a place known as Stardew Valley.

Stardew Valley is open-ended, allowing players to grow crops, raise
livestock, fish, cook, mine, forage, and socialize with the
townspeople, including the ability to marry and have children. It
allows up to eight players to play online together.")
    (license (undistributable "No URL"))))
