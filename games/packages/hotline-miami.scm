;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages hotline-miami)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (games packages mono)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (games utils)
  #:use-module (nonguix build utils)
  #:use-module (nongnu packages game-development)
  #:use-module (games humble-bundle))

(define-public hotline-miami
  (let* ((version "1392944501")
         (file-name (string-append "HotlineMiami_linux_" version ".tar.gz"))
         (binary "hotline_launcher"))
    (package
      (name "hotline-miami")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "01d8ifq529ipnli94dfvkacsdwingdnfz1cd2v7vafnnrgp6i0ik"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:system "i686-linux"
         #:patchelf-plan
         `(("Hotline"
            ("gcc" "openal" "glu" "mesa" "libvorbis" "libogg" "libx11" "nvidia-cg-toolkit"))
           (,,binary
            ("gcc" "mesa" "libxcb" "libxrandr" "libxi" "libxext" "libxrender" "libx11"
             "fontconfig" "freetype")))
         #:install-plan
         `(("." (".") "share/hotline-miami/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'deal-with-tarbomb
             (lambda _
               (chdir "..")
               #t))
           (add-after 'install 'clean-up-libs
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/hotline-miami/"))
                      (lib (string-append share "/lib/")))
                 (delete-file-recursively lib)
                 (delete-file (string-append share "README"))
                 (delete-file (string-append share "environment-variables"))
                 #t)))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/hotline-miami"))
                      (real (string-append output "/share/hotline-miami/" ,binary)))
                 (make-wrapper wrapper real)
                 (make-desktop-entry-file (string-append output "/share/applications/hotline-miami.desktop")
                                          #:name "Hotline Miami"
                                          #:exec wrapper
                                          ;; #:icon (string-append output "MISSING?.png")
                                          #:comment "Top-down shooter video game"
                                          #:categories '("Application" "Game")))
               #t)))))
      (inputs
       `(("gcc" ,gcc "lib")
         ("openal" ,openal)
         ("libxrandr" ,libxrandr)
         ("libx11" ,libx11)
         ("libxi" ,libxi)
         ("libxcb" ,libxcb)
         ("libxrender" ,libxrender)
         ("fontconfig" ,fontconfig)
         ("freetype" ,freetype)
         ("mesa" ,mesa)
         ("glu" ,glu)
         ("libvorbis" ,libvorbis)
         ("libogg" ,libogg)
         ("nvidia-cg-toolkit" ,nvidia-cg-toolkit)))
      (home-page "https://hotlinemiami.com/")
      (synopsis "Top-down shooter video game")
      (description "Hotline Miami is a top-down shooter video game set in 1989
Miami.  The game revolves primarily around an unnamed silent protagonist—dubbed
\"Jacket\" by fans—who has been receiving messages on his answering machine
instructing him to commit massacres against the local Russian mafia.

The game blends top-down perspective with stealth, extreme violence and surreal
storytelling, along with a soundtrack and visuals inspired by 1980s culture.  The
game itself was influenced in part by Nicolas Winding Refn's 2011 neo-noir crime
film Drive, as well as the 2006 documentary Cocaine Cowboys.")
      (license (undistributable "No URL")))))
