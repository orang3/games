;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages jamestown)
  #:use-module (ice-9 match)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (nongnu packages game-development)
  #:use-module (games humble-bundle))

(define-public jamestown
  (let* ((arch (match (or (%current-target-system)
                          (%current-system))
                 ;; TODO: The amd64 binary crashes patchelf, so we only build the x86 version for now.
                 ;; See https://github.com/NixOS/patchelf/issues/189.
                 ;; ("x86_64-linux" "amd64")
                 ("x86_64-linux" "x86")
                 ("i686-linux" "x86")
                 (_ ""))))
    (package
      (name "jamestown")
      (version "1.0.2")
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list (string-append "JamestownLinux_"
                                           (string-replace-substring version "." "_")
                                           ".zip")))))
         (file-name (string-append "JamestownLinux_"
                                   (string-replace-substring version "." "_")
                                   ".zip"))
         (sha256
          (base32
           "0v6ld0xl3nfa14nax3il10r3aq8hwy1xn22lrdz89yiklg4z48gd"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:system "i686-linux"
         #:patchelf-plan
         `((,,(string-append "data/Jamestown-" arch)
            ("gcc:lib" "mesa" "openal" "sdl" "libsteam")))
         #:install-plan
         `(("data/Archives" (".") "share/jamestown/Archives/")
           ("data" ("jamestown.png" "Jamestown_EULA.txt" "LICENSES.TXT"
                    "README-linux-generic.txt" "README-linux.txt")
            "share/jamestown/")
           ("data" (,,(string-append "Jamestown-" arch))  "share/jamestown/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "unzip")
                       (assoc-ref inputs "source"))
               ;; We use system* to ignore errors.
               (system* (which "unzip")
                        "JamestownInstaller_1_0_2-bin")
               #t))
           (add-after 'install 'make-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/jamestown"))
                      (real (string-append output "/share/jamestown/Jamestown-" ,arch))
                      (icon (string-append output "/share/jamestown/jamestown.png")))
                 (make-wrapper wrapper real)
                 (make-desktop-entry-file (string-append output "/share/applications/jamestown.desktop")
                                          #:name "Jamestown"
                                          #:exec wrapper
                                          #:icon icon
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("gcc:lib" ,gcc "lib")
         ("openal" ,openal)
         ("mesa" ,mesa)
         ("sdl" ,sdl)
         ("libsteam" ,libsteam)))
      (home-page "http://jamestownplus.com/")
      (synopsis "4-player cooperative shoot-em-up")
      (description
       "Jamestown: Legend of the Lost Colony, also known as simply Jamestown,
is a vertically scrolling shoot 'em up game that takes place on Mars in an
alternate history steampunk 17th century, where the planet is a British colony
contested by the Spanish and the indigenous Martians.

Jamestown features mechanics similar to many other shooters.  There are 4 types
of ships available, each with a primary and secondary attack, and a \"Vaunt Mode\"
which the player can trigger when enough gold from destroyed enemies has been
collected.  When this mode is engaged the ship gains a temporary shield,
increased firepower and a score multiplier for a limited time, which can be
prolonged and increased by collecting more gold.

The game is playable in both regular singleplayer mode and in a local
multiplayer mode supporting up to 4 players.")
      (license (undistributable "file://data/Jamestown_EULA.txt")))))
